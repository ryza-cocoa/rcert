# rcert

SSL certificates management and distribution with Rust.

![screenshot](screenshot.png)

## Usage

```bash
# Run as server
# Listening on all interfaces on port 12345, access secret is himitsu, and Redis connection URL
rcert server -l 0.0.0.0:12345 --secret himitsu --redis redis://127.0.0.1/

# Put SSL certs to rcert service
# Endpoint at 1.2.3.4:12345, access secret is himitsu
# Domain name (no checking) is ryza.moe
# Along with the path to public cert and private key
#
# And if succeeds, the program should print the version of the saved certs (e.g, a1b2c3d4e5)
rcert put -s 1.2.3.4:12345 --secret himitsu -d ryza.moe -c fullchain.pem -k privkey.pem

# Get SSL certs from rcert service
# Endpoint at 1.2.3.4:12345, access secret is himitsu
# Domain name (no checking) is ryza.moe, certs version is a1b2c3d4e5
# Along with the path to save public cert and private key
rcert get -s 1.2.3.4:12345 --secret himitsu -d ryza.moe -v a1b2c3d4e5 -c fullchain.pem -k privkey.pem

# List all SSL certs
rcert list -s 1.2.3.4:12345 --secret himitsu
```
